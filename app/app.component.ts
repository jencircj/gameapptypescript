import { Component, View } from "angular2/core";
import { OutcomeService } from "./http/http.service"
import { DialogComponent } from "./dialog/dialog.component"
import { WebServiceConstants } from './common/app.constants'
import 'rxjs/add/operator/catch'

@Component({
   selector: 'game-app',
   providers: [OutcomeService]
})

@View({
   templateUrl: 'app/app.component.html',
   styleUrls: ['app/app.component.css'],
   directives: [DialogComponent]
})

export class GameAppComponent {

	constructor(private outcomeService : OutcomeService){}
 
  //initialize default values
	outcomeData = {
                 'outcomeName': 'Big Win',
                 'randomNumbers': [0,0,0],
                 'bonusDetails': ''
               };
  //set default images path for random numbers 0,0,0
  imageUrl = this.outcomeService.createImagePaths(this.outcomeData.randomNumbers);
    
    //function below make server calls to get the outcome name
    loadOutcome() {
       this.outcomeService.getOutcome().subscribe(data => {
       	      this.outcomeData = data
               console.log('result ' + this.outcomeData.randomNumbers);
               this.imageUrl = this.outcomeService.createImagePaths(this.outcomeData.randomNumbers);               
         },
         err => {
           alert(WebServiceConstants.ERROR_MESSAGE);
         }
       );
    }

}