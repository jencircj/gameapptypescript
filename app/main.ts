import { bootstrap } from "angular2/platform/browser"
import { GameAppComponent } from "./app.component"
import { HTTP_PROVIDERS } from 'angular2/http';

bootstrap(GameAppComponent,[HTTP_PROVIDERS]);