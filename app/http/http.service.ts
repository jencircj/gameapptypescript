import { Injectable, Inject } from 'angular2/core';
import { Http, Response } from 'angular2/http';
import { Observable } from 'rxjs/Rx';
import { API_ENDPOINT, WebServiceConstants } from '../common/app.constants'
import 'rxjs/add/operator/map';

@Injectable()
export class OutcomeService {
  
  constructor (private http: Http) {

  }
  //Angular2 provider method that makes the web service call to get the outcomes 
  getOutcome() {
  	let reuestUrl = API_ENDPOINT + WebServiceConstants.GET_OUTCOME;
    return this.http.get(reuestUrl)
     .map((res) => res.json())
     .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  //Angular2 provider method that returns an array containing image paths 
  createImagePaths(randomNumbersArray){
         var imageUrls = [];
         
        randomNumbersArray.forEach((item, index) => {
            imageUrls[index] = API_ENDPOINT + 'Symbol_' + item +'.png';
        });

         return imageUrls
  }

}