export var API_ENDPOINT = 'http://127.0.0.1:3000/'

export class WebServiceConstants {
  public static GET_OUTCOME: string = "getOutcome";
  public static ERROR_MESSAGE: string = "Network Error: error fetching data from server, Please try later";
}

