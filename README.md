# README #

# Game Application 
## Version - 1.0.0 ##
This a sample game application that allows the player to generate an outcome from the outcomes listed below

* 'Big Win'
* 'Small Win'
* 'No Win'

Player can also earn bonus points as well based on his luck. The application is developed using Angular2- Typescript in front end and NodeJS Server for back end.

## Platforms Tested ##
Screenshots are provided in the platformscreenshots folder

* Google chrome
* Safari browser in Mac OS
* Mozilla firefox
* iPhone-6 Simulator (landscape and portrait modes)

## Pre-requisites for running the app ##
* **NodeJs** and **npm** installed

## How to Setup? ##

* Check out the code from repo to local directory.
* Move to the directory using terminal command **cd**
* Run command **npm install** , for Mac user's prefix the command with **sudo**, if required.
* All the dependencies for the project will now be installed in your directory
* Run **npm start** command to run the application