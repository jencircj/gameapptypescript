var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
System.register("common/app.constants", [], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var API_ENDPOINT, WebServiceConstants;
    return {
        setters:[],
        execute: function() {
            exports_1("API_ENDPOINT", API_ENDPOINT = 'http://127.0.0.1:3000/');
            WebServiceConstants = (function () {
                function WebServiceConstants() {
                }
                WebServiceConstants.GET_OUTCOME = "getOutcome";
                WebServiceConstants.ERROR_MESSAGE = "Network Error: error fetching data from server, Please try later";
                return WebServiceConstants;
            }());
            exports_1("WebServiceConstants", WebServiceConstants);
        }
    }
});
System.register("http/http.service", ['angular2/core', 'angular2/http', 'rxjs/Rx', "common/app.constants", 'rxjs/add/operator/map'], function(exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    var core_1, http_1, Rx_1, app_constants_1;
    var OutcomeService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (Rx_1_1) {
                Rx_1 = Rx_1_1;
            },
            function (app_constants_1_1) {
                app_constants_1 = app_constants_1_1;
            },
            function (_1) {}],
        execute: function() {
            OutcomeService = (function () {
                function OutcomeService(http) {
                    this.http = http;
                }
                //Angular2 provider method that makes the web service call to get the outcomes 
                OutcomeService.prototype.getOutcome = function () {
                    var reuestUrl = app_constants_1.API_ENDPOINT + app_constants_1.WebServiceConstants.GET_OUTCOME;
                    return this.http.get(reuestUrl)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
                };
                //Angular2 provider method that returns an array containing image paths 
                OutcomeService.prototype.createImagePaths = function (randomNumbersArray) {
                    var imageUrls = [];
                    for (var index = 0; index < randomNumbersArray.length; index++) {
                        imageUrls[index] = app_constants_1.API_ENDPOINT + 'Symbol_' + randomNumbersArray[index] + '.png';
                    }
                    return imageUrls;
                };
                OutcomeService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], OutcomeService);
                return OutcomeService;
            }());
            exports_2("OutcomeService", OutcomeService);
        }
    }
});
System.register("dialog/dialog.component", ["angular2/core"], function(exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    var core_2;
    var DialogComponent;
    return {
        setters:[
            function (core_2_1) {
                core_2 = core_2_1;
            }],
        execute: function() {
            DialogComponent = (function () {
                function DialogComponent() {
                }
                DialogComponent = __decorate([
                    core_2.Component({
                        selector: 'app-dialog',
                        inputs: ['bonusModal'],
                    }),
                    core_2.View({
                        templateUrl: 'app/dialog/dialog.component.html',
                        styleUrls: ['app/dialog/dialog.component.css']
                    }), 
                    __metadata('design:paramtypes', [])
                ], DialogComponent);
                return DialogComponent;
            }());
            exports_3("DialogComponent", DialogComponent);
        }
    }
});
System.register("app.component", ["angular2/core", "http/http.service", "dialog/dialog.component", "common/app.constants", 'rxjs/add/operator/catch'], function(exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    var core_3, http_service_1, dialog_component_1, app_constants_2;
    var GameAppComponent;
    return {
        setters:[
            function (core_3_1) {
                core_3 = core_3_1;
            },
            function (http_service_1_1) {
                http_service_1 = http_service_1_1;
            },
            function (dialog_component_1_1) {
                dialog_component_1 = dialog_component_1_1;
            },
            function (app_constants_2_1) {
                app_constants_2 = app_constants_2_1;
            },
            function (_2) {}],
        execute: function() {
            GameAppComponent = (function () {
                function GameAppComponent(outcomeService) {
                    this.outcomeService = outcomeService;
                    //initialize default values
                    this.outcomeData = {
                        'outcomeName': 'Big Win',
                        'randomNumbers': [0, 0, 0],
                        'bonusDetails': ''
                    };
                    //set default images path for random numbers 0,0,0
                    this.imageUrl = this.outcomeService.createImagePaths(this.outcomeData.randomNumbers);
                }
                //function below make server calls to get the outcome name
                GameAppComponent.prototype.loadOutcome = function () {
                    var _this = this;
                    this.outcomeService.getOutcome().subscribe(function (data) {
                        _this.outcomeData = data;
                        console.log('result ' + _this.outcomeData.randomNumbers);
                        _this.imageUrl = _this.outcomeService.createImagePaths(_this.outcomeData.randomNumbers);
                    }, function (err) {
                        alert(app_constants_2.WebServiceConstants.ERROR_MESSAGE);
                    });
                };
                GameAppComponent = __decorate([
                    core_3.Component({
                        selector: 'game-app',
                        providers: [http_service_1.OutcomeService]
                    }),
                    core_3.View({
                        templateUrl: 'app/app.component.html',
                        styleUrls: ['app/app.component.css'],
                        directives: [dialog_component_1.DialogComponent]
                    }), 
                    __metadata('design:paramtypes', [http_service_1.OutcomeService])
                ], GameAppComponent);
                return GameAppComponent;
            }());
            exports_4("GameAppComponent", GameAppComponent);
        }
    }
});
System.register("main", ["angular2/platform/browser", "app.component", 'angular2/http'], function(exports_5, context_5) {
    "use strict";
    var __moduleName = context_5 && context_5.id;
    var browser_1, app_component_1, http_2;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (http_2_1) {
                http_2 = http_2_1;
            }],
        execute: function() {
            browser_1.bootstrap(app_component_1.GameAppComponent, [http_2.HTTP_PROVIDERS]);
        }
    }
});
//# sourceMappingURL=app.js.map